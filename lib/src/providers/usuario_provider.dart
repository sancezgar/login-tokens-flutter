import 'dart:convert';

import 'package:formulariobloc/src/share_prefs/preferencias_usuario.dart';
import 'package:http/http.dart' as http;

class UsuarioProvider {

  final String _firebaseToken = 'AIzaSyDU1pg2h0tIpFGmtukNvEd0jndfHTMZG7g';
  final _prefs = new PreferenciasUsuario();

  
  /* ================================================== *
   * ==========  Crear nuevo usuario en el backend  ========== *
   * ================================================== */
  
  Future<Map<String,dynamic>> nuevoUsuario(String email, String password) async{
    
    return _api('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$_firebaseToken',email,password);

  }
  
  /* =======  End of Crear nuevo usuario en el backend  ======= */

  
  /* ================================================== *
   * ==========  Login  ========== *
   * ================================================== */
  
  Future<Map<String,dynamic>> login(String email, String password) async{
    
    return _api('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$_firebaseToken',email,password);

  }
  
  /* =======  End of Login  ======= */

  
  /* ================================================== *
   * ==========  consumimos el api para obtener la respuesta  ========== *
   * ================================================== */
  
  Future<Map<String,dynamic>> _api(String url,String email, String password)async{

    /* --------  Obtenemos los datos  --------*/
    
    final authData = {

      'email' : email,
      'password' : password,
      'returnSecureToken' : true

    };

    
    /* --------  enviamos los datos  --------*/
    
    final resp = await http.post(
      url,
      body:json.encode( authData )
    );
    
    /* --------  Recibimos los datos  --------*/    
    Map<String,dynamic> decodeResp = json.decode( resp.body );

    print(decodeResp);

    
    /* --------  Validamos la respuesta  --------*/
    //se almacena el token como si fuera un localstorage
    if( decodeResp.containsKey('idToken') ){

      _prefs.token = decodeResp['idToken'];

      return { 'ok' : true, 'token':decodeResp['idToken'] };

    }else{
      return {'ok':false, 'mensaje': decodeResp['error']['message']} ;
    }

  }  
  
  /* =======  End of consumimos el api para obtener la respuesta  ======= */


}