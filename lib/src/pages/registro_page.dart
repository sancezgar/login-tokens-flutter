import 'package:flutter/material.dart';
import 'package:formulariobloc/src/blocs/provider.dart';
import 'package:formulariobloc/src/providers/usuario_provider.dart';
import 'package:formulariobloc/utils/util.dart' as util;

class RegistroPage extends StatelessWidget {
  
  /* ================================================== *
   * ==========  Espacio para las variables globales  ========== *
   * ================================================== */
  
  final usuarioProvider = new UsuarioProvider();
  
  /* =======  End of Espacio para las variables globales  ======= */
  
  /* ================================================== *
   * ==========  Build de la aplicación  ========== *
   * ================================================== */
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _loginForm(context),
        ],
      )

    );
  }
  
  /* =======  End of Build de la aplicación  ======= */
  
  /* ================================================== *
   * ==========  Fondo   ========== *
   * ================================================== */
  
  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondoMorado = Container(

      height: size.height * 0.40,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors:<Color>[
            Color.fromRGBO(63, 63, 156, 1.0),
            Color.fromRGBO(90, 70, 178, 1.0)
          ]
        )
      ),
      
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)
      ),
    );

    return Stack(
      children: <Widget>[
        fondoMorado,
        Positioned(top:90,left: 30.0,child: circulo,),
        Positioned(top:-40,right: -30.0,child: circulo,),
        Positioned(bottom:-50,right: -10.0,child: circulo,),
        Positioned(bottom:120,right: 20.0,child: circulo,),

        Container(
          padding: EdgeInsets.only(top:80.0),
          child:Column(
            children: <Widget>[
              Icon(Icons.person_pin_circle,color: Colors.white,size: 100.0,),
              SizedBox(height: 10.0, width: double.infinity,),
              Text('Uriel Sánchez', style: TextStyle(color: Colors.white, fontSize: 25.0,)),
            ],
          )
        )

      ],
    );

  }
  
  /* =======  End of Fondo  ======= */
  
  
  /* ================================================== *
   * ==========  Caja de Login Form  ========== *
   * ================================================== */  
  Widget _loginForm(BuildContext context) {

    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child:Column(
        children: <Widget>[
          
          /* --------  Se utiliza para dar un padding para centrar el contenedor del formulario  --------*/
          
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
      
          
          /* --------  Inicia El contenedor del formulario  --------*/
          
          Container(
            width:size.width *0.80,
            padding: EdgeInsets.symmetric(vertical: 50.0),
            margin: EdgeInsets.symmetric(vertical: 30.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0,5.0),
                  spreadRadius: 3.0
                )
              ]
            ),
            child: Column(
              children: <Widget>[
                Text('Registro',style:TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0,),
                _crearEmail(bloc),
                SizedBox(height: 30.0,),
                _crearPassword(bloc),
                SizedBox(height: 30.0,),
                _crearBoton(bloc),
                
              ],
            ),
          ),

          /* --------  Termina el contenedor del formulario  --------*/
          
          
          /* --------  Texto olvido contraseña  --------*/
          FlatButton(
            onPressed: ()=>Navigator.pushReplacementNamed(context, 'login'),
            child: Text('¿Ya tiene cuenta?'),
          ),
          SizedBox(height: 100.0),

        ],
      ),
    );

  }
  
  /* =======  End of Caja de Login Form  ======= */
  
  

  
  /* ================================================== *
   * ==========  Widgets del Formulario  ========== *
   * ================================================== */
  
  /* ================================================== *
   * ==========  Creando el input del email  ========== *
   * ================================================== */
  
  Widget _crearEmail(LoginBloc bloc) {

    return StreamBuilder(
      stream:bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){

       return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon(Icons.alternate_email,color:Colors.deepPurple),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo electrónico',
              counterText: snapshot.data,
              errorText: snapshot.error
            ),
            onChanged: bloc.changeEmail
          ),

        );

      },
    );

  }

  /* =======  End of Creando el input del email  ======= */
  
  /* ================================================== *
   * ==========  Creando el input de la contraseña  ========== *
   * ================================================== */
  
  Widget _crearPassword(LoginBloc bloc) {

    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context,AsyncSnapshot snapshot){

        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              icon: Icon(Icons.vpn_key,color:Colors.deepPurple),
              labelText: 'Contraseña',
              counterText: snapshot.data,
              errorText: snapshot.error

            ),
            onChanged: bloc.changePassword,
          ),

        );

      }
    );
    
  }

  /* =======  End of Creando el input de la contraseña  ======= */
  
  /* ================================================== *
   * ==========  Creando el boton  ========== *
   * ================================================== */
  
  Widget _crearBoton(LoginBloc bloc) {

    return StreamBuilder(
      stream: bloc.formValidStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        

        return RaisedButton(
          child:Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical:15.0),
            child: Text('Registrar')
          ),
          shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 1.0,
          color: Colors.deepPurple,
          textColor: Colors.white,
          onPressed: (snapshot.hasData)? ()=> _register(bloc, context) : null,
        );

      },
    );
  }  
  
  
  /* =======  End of Creando el boton  ======= */
  
  /* ================================================== *
   * ==========  Realizando el registro  ========== *
   * ================================================== */
  
  _register(LoginBloc bloc, BuildContext context)async{

    Map info = await usuarioProvider.nuevoUsuario(bloc.email, bloc.password);

    if(info['ok']){

      Navigator.pushReplacementNamed(context, 'home');

    }else{

      util.mostrarAlerta(context,info['mensaje']);

    }

  }
  
  /* =======  End of Realizando el registro  ======= */

  
  /* =======  End of Widgets del Formulario  ======= */
  
  
}