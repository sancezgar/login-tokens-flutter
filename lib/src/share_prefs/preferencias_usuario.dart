import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario{

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._();

  factory PreferenciasUsuario(){
    return _instancia;
  }

  PreferenciasUsuario._();

  SharedPreferences _prefs;

  initPrefs() async{

    this._prefs = await SharedPreferences.getInstance();

  }

   //GET y SET del token

  get token{

    return _prefs.getString('token') ?? 'No hay token';

  }

  set token(String value){

    _prefs.setString('token',value);

  }

  //Get y Set de ultima página
  get ultimaPagina{

    return _prefs.getString('ultimaPagina') ?? 'login';

  }

  set ultimaPagina(String value){

    _prefs.setString('ultimaPagina',value);

  }


}