import 'package:flutter/material.dart';
import 'package:formulariobloc/src/blocs/provider.dart';
import 'package:formulariobloc/src/pages/home_page.dart';
import 'package:formulariobloc/src/pages/login_page.dart';
import 'package:formulariobloc/src/pages/producto_page.dart';
import 'package:formulariobloc/src/pages/registro_page.dart';

import 'src/share_prefs/preferencias_usuario.dart';
 
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {

  final prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    //print(prefs.token);
    return Provider(
      child:MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: prefs.ultimaPagina,
        routes: {
          'login'   : (context) => LoginPage(),
          'home'   : (context) => HomePage(),
          'producto'   : (context) => ProductoPage(),
          'registro'   : (context) => RegistroPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.deepPurple
        ),
      )
    );

     
  }
}